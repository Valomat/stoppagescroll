# stopPageScroll.js #

stopPageScroll.js is a leightweight plugin that allows you to stop an HTML page scroll on demand. Its purpose is to help developping handmade mobile responsive menus and navigations (even though it can also be used for desktop screen sizes). The plugin offers different methods that you can call when opening or closing your custom menu. It will prevent your page from scrolling behind the usual background overlay of opened mobile menus. You can also use it for lightboxes or any kind of module that creates similar pop-up content. 

# How to #

* Download the sources
* Add the js and css files to your HTML page : 

```
#!HTML

<link rel="stylesheet" href="src/stopPageScroll.css">
<script src="src/stopPageScroll.js" type="text/javascript" charset="utf-8"></script>
```
* Call the plugin when opening your menu; let's suppose you have a custom menu as such : 

```
#!HTML

<nav>
   /* Your mobile menu */
</nav>
<div class="menu-trigger"></div> // Button that opens your menu
```
in jQuery you will usually have something similar to : 

```
#!Javascript

$(document).ready(function() {

   $(".menu-trigger").click(function() {
      $("nav").toggleClass('open');
   });

});
```

Then you can use stopPageScroll this way :

```
#!Javascript
$(document).ready(function() {

   // Create the stopPageScroll page wrapper
   $pageScroll = $("body").stopPageScroll();

   $(".menu-trigger").click(function() {
      $pageScroll.toggle(); 
      $("nav").toggleClass('open');
   });

});
```
**NOTE:** At the moment, the plugin must be called on the body. If you have a more complex menu behaviour, please check the methods below

# Methods #
* forbid() : forbids the scroll of the page
* allow() : allows the scroll of the page
* isForbidden() : returns true if the scroll is currently forbidden
* toggle() : toggles between allowed and forbidden states
## Exemples ##
```
#!Javascript

$pageScroll = $("body").stopPageScroll();
$pageScroll.forbid();
$pageScroll.allow();
$pageScroll.toggle();
$pageScroll.isForbidden();
```