(function($) {
	$.fn.stopPageScroll = function() {
		// Classe du body qui interdit le scroll de la page
		var forbidScrollOnClass = "body-stop-page-scroll-on";
		// ID du container de page quand le scroll est bloqué
		var pageContainerId = "stop-page-scroll-on";
		// code HTML du container de page quand le scroll est bloqué
		var pageContainerHtml = '<div id="' + pageContainerId + '"></div>';
		
		// Stocker une fois les informations de distance de scroll
		if ( !$.hasData(document.body, "distanceTop") ){
			$.data(document.body, "distanceTop", -1);
		}

		(function(element){ 
		    setTimeout(function() { 
		        if ( !element.isForbidden() )
		        	element.wrapInner(pageContainerHtml);
		    }, 1);
		})(this);

		/*
			Interdit le scroll de la page. 
		*/
		this.forbid = function() {
			console.log("forbid starts");
			// Récupérer les informations de scroll
			var distanceTop = $.data(document.body, "distanceTop");

			// Si le scroll est bien autorisé
			if (distanceTop < 0) {
				// Récupérer la distance de scroll dans la page
				distanceTop = $(window).scrollTop();

				this.addClass(forbidScrollOnClass);
			
				// Positionner correctement le container pour donner l'impression que rien n'a bougé
				$("#" + pageContainerId).css("marginTop", - ( distanceTop ) + "px");

				$.data(document.body, "distanceTop", distanceTop);
			}
			console.log("forbid stops");
		}

		/*
			Autorise le scroll de la page. 
		*/
		this.allow = function() {
			console.log("allow starts");
			var distanceTop = $.data(document.body, "distanceTop");

			if (distanceTop >= 0) {
				this.removeClass(forbidScrollOnClass);
				$("#" + pageContainerId).css("marginTop", "0px");
				
				// Ramener le scroll de la page au bon endroit
				$(window).scrollTop(distanceTop);
				
				// La distance de scroll est de nouveau autorisée
				distanceTop = -1;
				$.data(document.body, "distanceTop", distanceTop);
			}
			console.log("allow stops");
		}

		this.isForbidden = function() {
			return this.hasClass(forbidScrollOnClass);
		}

		this.toggle = function () {
			
			// Interdire / autoriser le scroll
			if ( this.hasClass(forbidScrollOnClass) ) {
				this.allow();
			} else {
				this.forbid();
			}
		}

		return this;

	}

    

})(jQuery);